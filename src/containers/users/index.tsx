import React, { Component } from 'react';
import { IUserState } from '../../interfaces/user.interface';

class Users extends Component<{}, IUserState> {
	state = {
		loading: false,
		userData: [],
	};

	componentDidMount = async () => {
		this.generateData();
	};

	generateData = async () => {
		await this.setState({ loading: true });
		let docs: Array<any> = [];
		for (var i = 0; i < 100; i++) {
			var types = ['quiz', 'exam', 'homework'];
			for (var j = 0; j < 3; j++) {
				const temp = {
					student: i,
					type: types[j],
					score: Math.round(Math.random() * 100),
				};
				docs = [...docs, temp];
			}

			if (i === 99) {
				await this.setState({
					userData: [...this.state.userData, ...docs],
					loading: false,
				});

				console.log('User data: ', this.state.userData);
			}
		}
	};

	render() {
		return <p>Users works!</p>;
	}
}

export default Users;
