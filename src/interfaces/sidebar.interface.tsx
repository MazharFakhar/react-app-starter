export interface ISidebarComponentState {
	mobileOpen: boolean;
}

export interface IMenu {
	name: string;
	url?: string;
	children?: Array<IMenu>;
}
