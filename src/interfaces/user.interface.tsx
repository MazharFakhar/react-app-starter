interface User {
	name: number;
	type: string;
	score: number;
}

export interface IUserState {
	loading: boolean;
	userData: Array<User>;
}
