import React, { Component, Fragment } from 'react';
import { ILoginState, ILoginForm } from '../../interfaces/login.interface';
import LoginComponent from '../../component/login/login.component';
import { FormikActions } from 'formik';

class Login extends Component<{}, ILoginState | any> {
	state: Readonly<ILoginState> = {
		email: '',
		password: '',
	};

	onLogin = async (values: ILoginForm, actions: FormikActions<ILoginForm>) => {
		console.log({ values, actions });
		alert(JSON.stringify(values, null, 2));
		actions.setSubmitting(false);
	};

	render() {
		const { email, password } = this.state;
		return (
			<Fragment>
				<LoginComponent
					email={email}
					password={password}
					onLogin={this.onLogin}
				/>
			</Fragment>
		);
	}
}

export default Login;
