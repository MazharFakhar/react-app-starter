import React, { Component } from 'react';
import { MuiThemeProvider } from '@material-ui/core/styles';
import { Theme, CssBaseline } from '@material-ui/core';
import theme from './ui/theme';
import { AppRoutes } from './routes';

class App extends Component {
	render() {
		return (
			<MuiThemeProvider theme={theme as Theme}>
				<CssBaseline />
				<AppRoutes />
			</MuiThemeProvider>
		);
	}
}

export default App;
