import { FormEvent } from 'react';
import { FormikActions } from 'formik';

export interface ILoginState {
	email: string;
	password: string;
}

export interface ILoginComponentProps {
	email: string;
	password: string;
	onLogin: (values: ILoginForm, actions: FormikActions<ILoginForm>) => void;
}

export interface ILoginForm extends ILoginState {}
