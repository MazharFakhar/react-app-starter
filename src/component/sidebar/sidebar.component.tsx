import React, { PureComponent, Fragment } from 'react';
import AppBar from '@material-ui/core/AppBar';
import Hidden from '@material-ui/core/Hidden';
import IconButton from '@material-ui/core/IconButton';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import Collapse from '@material-ui/core/Collapse';
import ExpandLess from '@material-ui/icons/ExpandLess';
import ExpandMore from '@material-ui/icons/ExpandMore';
import Drawer from '@material-ui/core/Drawer';
import { withStyles, Theme, WithStyles } from '@material-ui/core/styles';
import { NavLink } from 'react-router-dom';
import MenuIcon from '@material-ui/icons/Menu';
import { AdminRoutes } from '../../routes';
import { ISidebarComponentState } from '../../interfaces/sidebar.interface.jsx';
import SidebarMenu from './sidebar-menu';

const drawerWidth = 240;
const styles = (theme: Theme) => ({
	root: {
		display: 'flex',
	},
	appBar: {
		zIndex: theme.zIndex.drawer + 1,
	},
	drawer: {
		[theme.breakpoints.up('sm')]: {
			width: drawerWidth,
			flexShrink: 0,
		},
	},
	drawerPaper: {
		width: drawerWidth,
	},
	links: {
		textDecoration: 'none',
	},
	menuHeader: {
		paddingLeft: '30px',
	},
	menuButton: {
		marginRight: 10,
		[theme.breakpoints.up('sm')]: {
			display: 'none',
		},
	},
	content: {
		flexGrow: 1,
		padding: theme.spacing(3),
	},
	toolbar: theme.mixins.toolbar,
});

class SidebarComponent extends PureComponent<
	WithStyles,
	ISidebarComponentState
> {
	constructor(props: WithStyles) {
		super(props);
		this.state = {
			mobileOpen: false,
		};
	}

	handleDrawerToggle = () => {
		this.setState((state: any) => ({ mobileOpen: !state.mobileOpen }));
	};

	handleClick = async (item: string): Promise<void> => {
		await this.setState((prevState: ISidebarComponentState) => ({
			[item as keyof ISidebarComponentState]: !prevState[
				item as keyof ISidebarComponentState
			],
		}));
	};

	handler(children: any) {
		const { classes } = this.props;
		const { state } = this;
		return children.map((subOption: any, index: number) => {
			if (!subOption.children) {
				return (
					<div key={index}>
						<NavLink to={subOption.url} className={classes.links}>
							<ListItem button>
								<ListItemText inset primary={subOption.name} />
							</ListItem>
						</NavLink>
					</div>
				);
			}
			return (
				<div key={index}>
					<ListItem button onClick={() => this.handleClick(subOption.name)}>
						<ListItemText inset primary={subOption.name} />
						{state[subOption.name as keyof ISidebarComponentState] ? (
							<ExpandLess />
						) : (
							<ExpandMore />
						)}
					</ListItem>
					<Collapse
						in={state[subOption.name as keyof ISidebarComponentState]}
						timeout='auto'
						unmountOnExit
					>
						{this.handler(subOption.children)}
					</Collapse>
				</div>
			);
		});
	}

	render() {
		const { classes } = this.props;
		const drawer = (
			<Fragment>
				<div className={classes.toolbar} />
				<List>
					<ListItem key='menuHeading' divider disableGutters>
						<ListItemText
							className={classes.menuHeader}
							inset
							primary='Nested Menu'
						/>
					</ListItem>
					{this.handler(SidebarMenu())}
				</List>
			</Fragment>
		);

		return (
			<div className={classes.root}>
				<AppBar position='fixed' className={classes.appBar}>
					<Toolbar>
						<IconButton
							color='inherit'
							aria-label='Open drawer'
							onClick={this.handleDrawerToggle}
							className={classes.menuButton}
						>
							<MenuIcon />
						</IconButton>
						<Typography variant='h6' color='inherit' noWrap>
							ADMIN PANEL
						</Typography>
					</Toolbar>
				</AppBar>
				<Hidden smUp implementation='css'>
					<Drawer
						className={classes.drawer}
						variant='temporary'
						anchor='left'
						open={this.state.mobileOpen}
						onClose={this.handleDrawerToggle}
						classes={{ paper: classes.drawerPaper }}
					>
						{drawer}
					</Drawer>
				</Hidden>
				<Hidden xsDown implementation='css'>
					<Drawer
						className={classes.drawer}
						variant='persistent'
						anchor='left'
						open
						classes={{ paper: classes.drawerPaper }}
					>
						{drawer}
					</Drawer>
				</Hidden>
				<main className={classes.content}>
					<div className={classes.toolbar} />
					<AdminRoutes />
				</main>
			</div>
		);
	}
}

export default withStyles(styles)(SidebarComponent);
