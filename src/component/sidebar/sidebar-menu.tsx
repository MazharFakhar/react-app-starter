import { IMenu } from '../../interfaces/sidebar.interface';

const SidebarMenu = (): IMenu[] => {
  const menu: Array<IMenu> = [
    { name: 'Dashboard', url: '/admin/dashboard' },
    { name: 'Users', url: '/admin/users' },
    {
      name: 'Analytics',
      children: [
        { name: 'Touched', url: '/' },
        { name: 'Liked', url: '/' },
        { name: 'Favourite', url: '/' }
      ]
    },
    {
      name: 'Retention',
      children: [
        {
          name: 'New',
          url: '/'
        },
        {
          name: 'Old',
          url: '/'
        },
        {
          name: 'Location',
          children: [
            {
              name: 'City',
              url: '/'
            },
            {
              name: 'Country',
              url: '/,'
            }
          ]
        }
      ]
    }
  ];

  return menu;
};

export default SidebarMenu;
