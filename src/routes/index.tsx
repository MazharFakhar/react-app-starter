import React from 'react';
import { Switch, Route } from 'react-router-dom';
import Login from '../containers/login';
import Sidebar from '../containers/sidebar';

import { IAdminRoutes, IAppRoutes } from '../interfaces/routes.interface';
import Dashboard from '../containers/dashboard';
import Users from '../containers/users';
import UserAdd from '../component/users/add';

const appRoutes: IAppRoutes[] = [
	{
		path: '/',
		exact: true,
		main: Login,
		id: 1,
	},
	{
		path: '/login',
		exact: true,
		main: Login,
		id: 2,
	},
	{
		path: '/admin',
		exact: false,
		main: Sidebar,
		id: 4,
	},
];

const initialRoute: string = '/admin';
const adminRoutes: IAdminRoutes[] = [
	{
		path: initialRoute + '/dashboard',
		exact: true,
		main: Dashboard,
		name: 'Dashboard',
		id: 11,
	},
	{
		path: initialRoute + '/users',
		exact: true,
		main: Users,
		name: 'Users',
		id: 12,
	},
	{
		path: initialRoute + '/users/add',
		exact: true,
		main: UserAdd,
		name: 'Add User',
		id: 13,
	},
];

const AppRoutes = () => {
	return (
		<Switch>
			{appRoutes.map(r => (
				<Route key={r.id} path={r.path} exact={r.exact} component={r.main} />
			))}
		</Switch>
	);
};

const AdminRoutes = () => {
	return (
		<Switch>
			{adminRoutes.map(r => (
				<Route key={r.id} path={r.path} exact={r.exact} component={r.main} />
			))}
		</Switch>
	);
};

export { AppRoutes, AdminRoutes };
