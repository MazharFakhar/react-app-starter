import React, { ChangeEvent } from 'react';
import Button from '@material-ui/core/Button';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import Input from '@material-ui/core/Input';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import { ILoginForm } from '../../interfaces/login.interface';
import { FormikProps } from 'formik';
import FormHelperText from '@material-ui/core/FormHelperText';

const LoginForm = (props: FormikProps<ILoginForm>) => {
	const {
		values: { email, password },
		errors,
		touched,
		handleSubmit,
		handleChange,
		setFieldTouched,
	} = props;

	const change = (
		name: any,
		e: ChangeEvent<HTMLInputElement | HTMLTextAreaElement>,
	) => {
		e.persist();
		handleChange(e);
		setFieldTouched(name, true, false);
	};

	return (
		<form className='login-form' onSubmit={handleSubmit} noValidate={true}>
			<FormControl margin='dense' required fullWidth>
				<InputLabel htmlFor='email'>Email Address</InputLabel>
				<Input
					name='email'
					type='email'
					id='email'
					value={email}
					fullWidth
					autoComplete='email'
					error={touched.email && Boolean(errors.email)}
					onChange={change.bind(null, 'email')}
				/>
				<FormHelperText style={{ color: 'red' }}>
					{touched.email ? errors.email : ''}
				</FormHelperText>
			</FormControl>
			<FormControl margin='dense' required fullWidth>
				<InputLabel htmlFor='password'>Password</InputLabel>
				<Input
					name='password'
					type='password'
					id='password'
					value={password}
					fullWidth
					autoComplete='current-password'
					error={touched.password && Boolean(errors.password)}
					onChange={change.bind(null, 'password')}
				/>
				<FormHelperText style={{ color: 'red' }}>
					{touched.password ? errors.password : ''}
				</FormHelperText>
			</FormControl>
			<FormControlLabel
				control={<Checkbox value='remember' color='primary' />}
				label='Remember me'
			/>
			<Button type='submit' fullWidth variant='contained' color='primary'>
				Sign in
			</Button>
		</form>
	);
};

export default LoginForm;
