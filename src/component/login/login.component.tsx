import React, { PureComponent, Fragment } from 'react';
import Paper from '@material-ui/core/Paper';
import Avatar from '@material-ui/core/Avatar';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Typography from '@material-ui/core/Typography';
import { ILoginComponentProps } from '../../interfaces/login.interface';
import LoginForm from './login.form';
import { Formik, FormikActions } from 'formik';
import * as Yup from 'yup';

class LoginComponent extends PureComponent<ILoginComponentProps> {
	render() {
		const { email, password, onLogin } = this.props;
		const values = { email: email, password: password };
		const validationSchema = Yup.object({
			email: Yup.string()
				.email('Enter a valid email')
				.required('Email is required'),
			password: Yup.string()
				.min(5, 'Password must contain at least 5 characters')
				.max(18, 'Password cannot be more than 18 characters')
				.required('Enter your password'),
		});

		return (
			<Fragment>
				<div className='login-container'>
					<Paper className='login-paper' elevation={8}>
						<Avatar className='login-avatar'>
							<LockOutlinedIcon />
						</Avatar>
						<Typography component='h1' variant='h5' className='text-center'>
							Sign in
						</Typography>
						<Formik
							render={formikBag => <LoginForm {...formikBag} />}
							initialValues={values}
							validationSchema={validationSchema}
							onSubmit={onLogin}
						/>
					</Paper>
				</div>
			</Fragment>
		);
	}
}

export default LoginComponent;
