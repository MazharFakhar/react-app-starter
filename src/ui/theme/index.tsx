import { createMuiTheme } from '@material-ui/core/styles';

const palette = {
	primary: { main: '#8E24AA' },
	secondary: { main: '#00796B' },
};
const themeName = 'Seance Pine Green Camel';

export default createMuiTheme({ palette });
