import React, { Component } from 'react';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import classes from '*.module.scss';

const useStyles = makeStyles((theme: Theme) =>
	createStyles({
		root: {
			flexGrow: 1,
		},
		title: {
			flexGrow: 1,
		},
	}),
);

class UserAdd extends Component<{}, {}> {
	render() {
		return (
			<div>
				<Typography
					paragraph={false}
					variant='h5'
					component='h2'
					color='textPrimary'
				>
					Add User
				</Typography>
			</div>
		);
	}
}

export default UserAdd;
